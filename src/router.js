import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './views/Home'
import Tags from './views/Tags'
import Post from './views/Post'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/tags/:tag',
      name: 'tags',
      component: Tags
    },
    {
      path: '/posts/:post',
      name: 'post',
      component: Post
    },
  ],
})
